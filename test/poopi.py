import FWCore.ParameterSet.Config as cms

from CommonTools.PileupAlgos.Puppi_cff import puppi
from RecoMET.METProducers.PFMET_cfi import pfMet

process = cms.Process('POOPI')

# process.Timing = cms.Service("Timing")

process.load("FWCore.MessageService.MessageLogger_cfi")
process.MessageLogger.cerr.FwkReport.reportEvery = 10

process.maxEvents =  cms.untracked.PSet(
      input = cms.untracked.int32(100)
)

process.source = cms.Source(
    "PoolSource",
    fileNames = cms.untracked.vstring('file:/local/snarayan/aod_tt.root')
)

process.pfCandNoLep = cms.EDFilter("CandPtrSelector", 
    src = cms.InputTag("particleFlow"), 
    cut = cms.string("abs(pdgId) != 13 && abs(pdgId) != 11 && abs(pdgId) != 15")
)

process.pfCandLep = cms.EDFilter("CandPtrSelector", 
    src = cms.InputTag("particleFlow"), 
    cut = cms.string("abs(pdgId) == 13 || abs(pdgId) == 11 || abs(pdgId) == 15")
)

process.puppiNoLep = puppi.clone()
process.puppiNoLep.candName = 'pfCandNoLep'

process.puppiNoLepPlusLep = cms.EDProducer('CandViewMerger',
    src = cms.VInputTag('puppiNoLep', 'pfCandLep')
)

process.puppiMet = pfMet.clone()
process.puppiMet.src = cms.InputTag('puppiNoLepPlusLep')
process.puppiMet.calculateSignificance = False

process.puppiSequence = cms.Sequence(
    process.pfCandNoLep +
    process.pfCandLep +
    process.puppiNoLep + 
    process.puppiNoLepPlusLep +
    process.puppiMet
)

process.path = cms.Path(
    process.puppiSequence
    # *puppiMetSequence
)
